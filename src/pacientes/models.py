from django.db import models

class IngresoPacientes(models.Model):
    nombre=models.CharField(max_length=100, blank=False,null=False )
    email=models.EmailField()
    fecha= models.DateTimeField(auto_now_add=False,auto_now=False)
    cargas_familiares=models.IntegerField(max_length=4)

    def __str__(self):
        return self.nombre


