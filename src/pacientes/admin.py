from django.contrib import admin
from .models import IngresoPacientes

class AdminPacientes(admin.ModelAdmin):
    list_display = ["nombre","email","fecha","cargas_familiares"]
    list_filter = ["fecha"]

    #list_editable = ["nombre"]
    search_fields = ["nombre", "email"]

    #class  Meta:
       # model = IngresoPacientes


admin.site.register(IngresoPacientes,AdminPacientes)

# Register your models here.
